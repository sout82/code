package main

import (
  "net/http"
  "reflect"
  "encoding/json"
  "log"
  "fmt"
  "io/ioutil"
  "github.com/smallfish/simpleyaml"
  "github.com/gorilla/mux"
)

//TYPES

type Environments struct {
  Dev string `json:"dev"`
  Pre string `json:"pre"`
  Pro string `json:"pro"`
}

type DeployState struct {
  Deployed Environments `json:"deployed"`
  To_Deploy Environments `json:"to_deploy"`
}

type Manifest struct {
  Deployable  map[string]DeployState `json:"deployable"`
}

type Component struct {
  Id string `json:"id"`
  Name string `json:"name"`
  Description string `json:"description"`
  Manifest string `json:"manifest"`
  Path string `json:"path"`
  Versions DeployState `json:"versions"`
}

type ComponentList struct {
  Components []string `json:"components"`
}
type ApplicationComponentList struct {
  Applications map[string]ComponentList `json:"applications"`
}

type ApplicationList struct {
  Applications []string `json:"applications"`
}

type Application struct {
  Name string `json:"name"`
  Id string `json:"id"`
  Description string `json:"description"`
  Components map[string]Component `json:"components"`
}

type Applications struct {
  Applications map[string]Application `json:"applications"`
}

//HANDLERS AND FUNCTIONS

/*
Description: parse manifest file
*/

func parseManifest(path string) (Manifest,string){
  var appsCol Manifest 
  appsCol.Deployable = make(map[string]DeployState)
  appFile := path 
  fmt.Println(path)
  fileContent, err := ioutil.ReadFile(appFile)
  if err != nil {
    log.Printf("yamlFile.Get err   #%v ", err)
  }
  yaml, err := simpleyaml.NewYaml(fileContent)
  if err != nil {
        // ERROR
  }
  apps,_ := yaml.GetMapKeys()
  var cname string
  for _,app := range apps {
    fmt.Println(app)
    //appyaml := yaml.Get(app)
    var appStruct DeployState
    var environments Environments
    dev,_ := yaml.Get(app).Get("deployed").Get("dev").String()
    environments.Dev = dev
    pre,_ := yaml.Get(app).Get("deployed").Get("pre").String()
    environments.Pre = pre
    pro,_ := yaml.Get(app).Get("deployed").Get("pro").String()
    environments.Pro = pro
    appStruct.Deployed = environments
    dev,_ = yaml.Get(app).Get("to_deploy").Get("dev").String()
    environments.Dev = dev
    pre,_ = yaml.Get(app).Get("to_deploy").Get("pre").String()
    environments.Pre = pre
    pro,_ = yaml.Get(app).Get("to_deploy").Get("pro").String()
    environments.Pro = pro
    appStruct.To_Deploy = environments
    appsCol.Deployable[app] = appStruct
    cname = app
  }
  return appsCol,cname
}

/*
Description: parse file with list of applications and components
*/

func parseApplications() Applications{
  var appsCol Applications
  appsCol.Applications = make(map[string]Application)
  appFile := "applications.yaml"
  fileContent, err := ioutil.ReadFile(appFile)
  if err != nil {
    log.Printf("yamlFile.Get err   #%v ", err)
  }
  yaml, err := simpleyaml.NewYaml(fileContent)
  if err != nil {
        // ERROR
  }
   
  apps,_ := yaml.Get("applications").GetMapKeys()
  for _,app := range apps {
    appyaml := yaml.Get("applications").Get(app)
    var appStruct Application
    appStruct.Id = app
    appStruct.Name,_ = appyaml.Get("name").String()
    appStruct.Description,_ = appyaml.Get("description").String()
    appStruct.Components = make(map[string]Component)
    coms,_ := appyaml.Get("components").GetMapKeys()
    for _,com := range coms {
      var comStruct Component
      comyaml := yaml.Get("applications").Get(app).Get("components").Get(com)
      comStruct.Id = com
      comStruct.Name,_ = comyaml.Get("name").String()
      comStruct.Description,_ = comyaml.Get("description").String()
      comStruct.Manifest,_ = comyaml.Get("manifest").String()
      comStruct.Path,_ = comyaml.Get("path").String()
      if _, ok := manifests[comStruct.Path]; ok {
        fmt.Println("CACHED")
        comStruct.Versions=manifests[comStruct.Path]
      }else{
        fmt.Println("NOT CACHED")
	manifest,app:=parseManifest(comStruct.Path)
        ds:=manifest.Deployable[app]
        manifests[comStruct.Path]=ds
        comStruct.Versions=manifest.Deployable[app]
      }
      appStruct.Components[com] = comStruct
    }
    appsCol.Applications[app] = appStruct
  }
  return appsCol
}

func downloadManifest(){
  //accessToken := "zyQPABu5Py9dzrM7s4Fc"
  url := "https://gitlab.com/sout82/test/raw/master/manifest/manifest.yaml"
  client := &http.Client{}
  req, _ := http.NewRequest("GET", url, nil)
  req.Header.Set("PRIVATE-TOKEN", "zyQPABu5Py9dzrM7s4Fc")
  res, _ := client.Do(req)
  defer res.Body.Close()
  bodyBytes, _ := ioutil.ReadAll(res.Body)
  bodyString := string(bodyBytes)
  fmt.Println(bodyString)
}

/*
Description: init function
*/

func load() Applications {
  return parseApplications()   
}

/*
Description: get list of applications
*/

var  getList = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("GETAPPLICATIONLIST")
  pappsCol := applications
  keys := reflect.ValueOf(pappsCol.Applications).MapKeys()
  strkeys := make([]string, len(keys))
  for i := 0; i < len(keys); i++ {
    strkeys[i] = keys[i].String()
  }
  var applicationList ApplicationList
  applicationList.Applications = strkeys
  jsonStruct, err := json.Marshal(applicationList)
  if err != nil {
    fmt.Printf("Error: %s", err)
    return;
  }
  w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusOK)
  fmt.Fprintf(w,"%s", jsonStruct)
})

/*
Description: get list of components of application
*/

var  getComponentList = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("GETCOMPONENTLIST")
  pappsCol := applications
  vars := mux.Vars(r)
  application := vars["application"]
  fmt.Println(pappsCol.Applications[application].Components)
  keys := reflect.ValueOf(pappsCol.Applications[application].Components).MapKeys()
  strkeys := make([]string, len(keys))
  var appcomponentlist ApplicationComponentList
  appcomponentlist.Applications = make(map[string]ComponentList)
  for i := 0; i < len(keys); i++ {
    strkeys[i] = keys[i].String()
  }
  fmt.Println("1")
  fmt.Println(strkeys)
  var comlist ComponentList 
  comlist.Components = strkeys
  appcomponentlist.Applications[application] = comlist
  jsonStruct, err := json.Marshal(appcomponentlist)
  if err != nil {
    fmt.Printf("Error: %s", err)
    return;
  }
  w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusOK)
  fmt.Fprintf(w,"%s", jsonStruct)
})

/*
Description: get info from all components
*/

var  getAll = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("GETALL")
  pappsCol := applications 
  jsonStruct, err := json.Marshal(pappsCol)
  if err != nil {
    fmt.Printf("Error: %s", err)
    return;
  }
  w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusOK)
  fmt.Fprintf(w,"%s", jsonStruct)
})

/*
Description: refresh manifest info of all components
*/

var doRefreshAll = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("REFRESH ALL")
  applications = parseApplications()
})

/*
Description: refresh manifest info of components application
*/

var doRefreshApplication = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("DO REFRESH APP")
  vars := mux.Vars(r)
  application := vars["application"]
  aux := applications.Applications[application].Components
  keys := reflect.ValueOf(aux).MapKeys()
  strkeys := make([]string, len(keys))
  for i := 0; i < len(keys); i++ {
    strkeys[i] = keys[i].String()
  }
  for _,newkey := range strkeys{
    path := applications.Applications[application].Components[newkey].Path
    manifest,app:=parseManifest(path)
    ds:=manifest.Deployable[app]
    manifests[path]=ds
  }
})

/*
Description: refresh manifest info of component
*/

var doRefreshComponent = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("DO REFRESH COMPONENT")
  vars := mux.Vars(r)
  application := vars["application"]
  component := vars["component"]
  path := applications.Applications[application].Components[component].Path  
  manifest,app:=parseManifest(path)
  ds:=manifest.Deployable[app]
  manifests[path]=ds
})

/*
Description: get application info
*/

var  getApplication = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("GETAPPLICATION")
  vars := mux.Vars(r)
  application := vars["application"]
  var rapplications Applications
  rapplications.Applications = make(map[string]Application)
  rapplications.Applications[application] = applications.Applications[application]
  jsonStruct, err := json.Marshal(rapplications)
  if err != nil {
    fmt.Printf("Error: %s", err)
    return;
  }
  w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusOK)
  fmt.Fprintf(w,"%s", jsonStruct)

})

/* 
Description: get component info from application
*/

var  getComponent = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("GETCOMPONENT")
  vars := mux.Vars(r)
  application := vars["application"]
  component := vars["component"]
  w.WriteHeader(http.StatusOK)
  var rapplications Applications
  var newapp Application
  var com Component
  rapplications.Applications = make(map[string]Application)
  newapp.Id = applications.Applications[application].Id
  newapp.Name = applications.Applications[application].Name
  newapp.Description = applications.Applications[application].Description
  fmt.Println(newapp)
  com.Id = applications.Applications[application].Components[component].Id
  com.Name = applications.Applications[application].Components[component].Name
  com.Description = applications.Applications[application].Components[component].Description
  com.Manifest = applications.Applications[application].Components[component].Manifest
  com.Path = applications.Applications[application].Components[component].Path
  newapp.Components = make(map[string]Component)
  if _, ok := manifests[com.Path]; ok {
    fmt.Println("CACHED")
    com.Versions=manifests[com.Path]
  }else{
    fmt.Println("NOT CACHED")
    manifest,app:=parseManifest(com.Path)
    ds:=manifest.Deployable[app]
    manifests[com.Path]=ds
    com.Versions=manifest.Deployable[app]
  }
  newapp.Components[component] = com
  rapplications.Applications[application] = newapp 
    
  
  jsonStruct, err := json.Marshal(rapplications)
  if err != nil {
    fmt.Printf("Error: %s", err)
    return;
  }
  w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusOK)
  fmt.Fprintf(w,"%s", jsonStruct)
})

var  getStatus = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
  fmt.Println("DO STATUS")
})


//MAIN

var applications Applications
var manifests map[string]DeployState
var mode = "file"

func main(){

  manifests = make(map[string]DeployState)
  applications = parseApplications()
  router := mux.NewRouter()
  
  router.HandleFunc("/api/v1/manifest/application/refresh",doRefreshAll).Methods("GET") 
  router.HandleFunc("/api/v1/manifest/application/{application}/refresh",doRefreshApplication).Methods("GET") 
  router.HandleFunc("/api/v1/manifest/application/{application}/component/{component}/refresh",doRefreshComponent).Methods("GET") 
  router.HandleFunc("/api/v1/manifest/application/list",getList).Methods("GET") 
  router.HandleFunc("/api/v1/manifest/application/{application}/component/list",getComponentList).Methods("GET") 
  router.HandleFunc("/api/v1/manifest/application",getAll).Methods("GET")
  router.HandleFunc("/api/v1/manifest/application/{application}",getApplication).Methods("GET")
  router.HandleFunc("/api/v1/manifest/application/{application}/component/{component}",getComponent).Methods("GET")
  router.HandleFunc("/api/v1/status",getStatus).Methods("GET")

  log.Fatal(http.ListenAndServe(":8000", router))
  
}
